#!/usr/bin/env bash
set -e
ARCH=${ARCH} CROSS_COMPILE=${CROSS_COMPILE} make ${DEFCONFIG}
make menuconfig
ARCH=${ARCH} CROSS_COMPILE=${CROSS_COMPILE} make -j ${nproc}
rm -rf build/${DEFCONFIG}/
mkdir -p build/${DEFCONFIG}/.
cp u-boot* build/${DEFCONFIG}/.
