ARG ARCH=arm64
ARG CROSS_COMPILE=aarch64-linux-gnu-
ARG DEFCONFIG=p3450-0000_defconfig

ARG HOME=/root
ARG SRCDIR=.
ARG PKGDIR=${HOME}/git/u-boot

FROM archlinux:latest as builder
# archlinux required packages
RUN rm -r /etc/pacman.d/gnupg && \
    pacman-key --init && \
    pacman-key --populate && \
    pacman -Sy archlinux-keyring --noconfirm && pacman -Syyuu --noconfirm
RUN pacman -S --noconfirm \
    aarch64-linux-gnu-gcc \
    bash \
    base-devel \
    bc \
    bison \
    flex \
    gawk \
    gcc \
    git \
    make \
    ncurses \
    perl \
    python \
    texinfo \
    tree \
    xz
# Args late so they do not retrigger packaging if just customising
ARG ARCH
ARG CROSS_COMPILE
ARG DEFCONFIG
ARG HOME
ARG SRCDIR
ARG PKGDIR
# populating U-Boot files
RUN mkdir -p ${PKGDIR}
COPY ${SRCDIR} ${PKGDIR}/.
WORKDIR ${PKGDIR}
# populating the entrypoint file
RUN envsubst < ./scripts/docker-entrypoint.sh > entrypoint.sh && \
    chmod 744 entrypoint.sh
CMD ./entrypoint.sh
